const { BASE_FEE, Asset, Keypair, Operation, Networks, Server, TransactionBuilder, xdr } = require('stellar-sdk');
const BigNumber = require('bignumber.js');
const server = new Server('https://horizon-testnet.stellar.org');

// load private keys
const config = require('./config.json');
const quest_account = Keypair.fromSecret(config.keys.quest_account); // distributor account
const custom_account = Keypair.fromSecret(config.keys.custom_account); // issuer account
const test_account = Keypair.fromSecret(config.keys.test_account);

// define custom asset
const HVLY = new Asset('HVLY', custom_account.publicKey());
const MAX_INT64 = '9223372036854775807';

// misc
const DEFAULT_TIMEOUT = 30;


// Create and fund a new account
function createAccount(privateKey, startingBalance) {
  console.log('createAccount()');
  let new_account = privateKey ?
    Keypair.fromSecret(privateKey) :
    Keypair.random();
  address = new_account.publicKey();
  startingBalance = startingBalance || '1';
  console.log('Creating account', address, 'with starting balance of', startingBalance);

  server
    .loadAccount(address)
    .then(account => {
      console.log('Account', address, 'already exists!');
      // TODO: print out current account balances
      return;
    })
    .catch(error => {
      console.log('Creating random friendbot account...');
      let friendbot_account = Keypair.random();

      server
        .friendbot(friendbot_account.publicKey())
        .call()
        .then(() => {
          console.log('Friendbot account created!');
          return server.loadAccount(friendbot_account.publicKey());
        })
        .then(friendbot => {
          console.log('Building create account transaction...');
          let transaction = new TransactionBuilder(friendbot, {
            fee: BASE_FEE,
            networkPassphrase: Networks.TESTNET
          })
          .addOperation(Operation.createAccount({
            destination: address,
            startingBalance: startingBalance
          }))
          .setTimeout(DEFAULT_TIMEOUT)
          .build();

          transaction.sign(friendbot_account);
          console.log('Create account transaction built and signed!');
          console.log('Submitting transaction...');
          return server.submitTransaction(transaction);
        })
        .then(result => {
          logSuccess(result);
          console.log('Account public key:', new_account.publicKey());
          console.log('Account private key:', new_account.secret());
        })
        .catch(logError);
    })
}


function sendPayment(sourceKeypair, toAddress, asset, amount) {
  console.log('sendPayment()');
  console.log('Sending', amount, asset.getCode(), 'to', toAddress, 'from', sourceKeypair.publicKey());
  console.log('Confirming account', toAddress, 'exists...');
  server
    .loadAccount(toAddress)
    .catch(error => {
      if (error instanceof StellarSdk.NotFoundError) {
        console.error('Account', toAddress, 'does not exist!');
      } else {
        return error;
      }
    })
    .then(() => {
      console.log('Account', toAddress, 'exists!');
      console.log('Loading source account...');
      return server.loadAccount(sourceKeypair.publicKey());
    })
    .then(sourceAccount => {
      console.log('Source account loaded!');
      console.log('Building payment transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET
      })
      .addOperation(Operation.payment({
        destination: toAddress,
        asset: asset,
        amount: amount
      }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(sourceKeypair);
      console.log('Payment transaction built and signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(logSuccess)
    .catch(logError);
}

function manageData(sourceKeypair, key, value) {
  console.log('manageData()');
  key = key || 'Hello';
  value = value || 'World';
  console.log('Type of \'key\':', typeof(key));
  console.log('Setting data of key:', key, 'and value:', value, 'on account', sourceKeypair.publicKey());

  server
    .loadAccount(sourceKeypair.publicKey())
    .then(sourceAccount => {
      console.log('Building manage data transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET
      })
      .addOperation(Operation.manageData({
        name: key,
        value: value
        }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(sourceKeypair);
      console.log('Manage data transaction signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(logSuccess)
    .catch(logError);
}

function addMultiSig(sourceKeypair, additionalAddress, additionalWeight) {
  console.log('addMultisig()');
  // TODO: Check if additionalAddress is already a signer on sourceAccount

  console.log('Adding', additionalAddress, 'as signer with weight', additionalWeight, 'to', sourceKeypair.publicKey());

  server
    .loadAccount(sourceKeypair.publicKey())
    .then(sourceAccount => {
      console.log('Building set options transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET
      })
      .addOperation(Operation.setOptions({
        // masterWeight: 2,
        // lowThreshold: 1,
        // medThreshold: 2,
        // highThreshold: 2,
        signer: {
          ed25519PublicKey: additionalAddress,
          weight: additionalWeight
        }
      }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(sourceKeypair);
      console.log('Set options transaction built and signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(logSuccess)
    .catch(logError);
}

function bumpSequence(sourceKeypair, bumpTo, signerKeypair) {
  console.log('bumpSequence()');
  bumpTo = bumpTo || '0';
  signer = signerKeypair || sourceKeypair;

  server
    .loadAccount(sourceKeypair.publicKey())
    .then(sourceAccount => {
      console.log('Building bump sequence transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET
      })
      .addOperation(Operation.bumpSequence({
        bumpTo: bumpTo
      }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(signer);
      console.log('Bump sequence transaction built and signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(logSuccess)
    .catch(logError);
}

function setThresholdsAndWeight(sourceKeypair, low, med, high, weight) {
  // TODO: Define setThresholdsAndWeight function
}

function setTrustline(sourceKeypair, asset, amount) {
  console.log('setTrustline()');
  if (!sourceKeypair || !asset) {
    throw new Error('you must specify a source keypair and asset!');
  }
  logAmount = amount || new BigNumber(MAX_INT64).toString();

  console.log('Setting trustline for', asset.getCode(), 'in the amount of', logAmount, 'on account', sourceKeypair.publicKey());

  server
    .loadAccount(sourceKeypair.publicKey())
    .then(sourceAccount => {
      console.log('Building change trust transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET,
      })
      .addOperation(Operation.changeTrust({
        asset: asset,
        limit: amount,
      }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(sourceKeypair);
      console.log('Change trust transaction built and signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(result => {
      logSuccess(result);
      console.log('Trustline established on', sourceKeypair.publicKey(), 'for', logAmount, asset.getCode());
    })
    .catch(logError);
}

function manageSellOffer(sourceKeypair, assetToSell, assetToBuy, amount, price, offerId) {
  console.log('manageSellOffer()');

  if (!assetToSell || !assetToBuy || !amount || !price) {
    throw new Error('You must specify a sell asset, buy asset, amount, price, and offer id!');
  }

  server
    .loadAccount(sourceKeypair.publicKey())
    .then(sourceAccount => {
      console.log('Building manage sell offer transaction...');
      let transaction = new TransactionBuilder(sourceAccount, {
        fee: BASE_FEE,
        networkPassphrase: Networks.TESTNET
      })
      .addOperation(Operation.manageSellOffer({
        selling: assetToSell,
        buying: assetToBuy,
        amount: amount,
        price: price,
        offerId: offerId
      }))
      .setTimeout(DEFAULT_TIMEOUT)
      .build();

      transaction.sign(sourceKeypair);
      console.log('Manage sell offer transaction built and signed!');
      console.log('Submitting transaction...');
      return server.submitTransaction(transaction);
    })
    .then(logSuccess)
    .catch(logError);
}

function logSuccess(result) {
  console.log('Success!');
  console.log('Transaction details:', 'https://stellar.expert/explorer/testnet/tx/' + result.id);
  // console.log('Envelope XDR:',
  //   JSON.stringify(xdr.TransactionEnvelope.fromXDR(result.envelope_xdr, 'base64'), null, 2)
  // );
  //
  // console.log('Result XDR:',
  //   JSON.stringify(xdr.TransactionResult.fromXDR(result.result_xdr, 'base64'), null, 2)
  // );
  //
  // console.log('Result Meta XDR:',
  //   JSON.stringify(xdr.TransactionMeta.fromXDR(result.result_meta_xdr, 'base64'), null, 2)
  // );
}

function logError(error) {
  console.error(error);
  try {
    console.error('Extras:', error.response.data.extras);
  } catch (e) {
    console.log('No error.response.data.extras :(');
  }
}

function main() {
  console.log('Here we go...');

  // challenge 01 - Create and fund a Stellar account
  // createAccount(quest_account.secret(), '1000');
  // createAccount('SBPIUYBFZETWOG6KZNGG4PDVHKYWX2W76ADDAK4KW4ET53Z23KE3FDKO', '1000');

  // challenge 02 - Make a payment from your Stellar account
  // sendPayment(quest_account, test_account.publicKey(), Asset.native(), '10');

  // challenge 03 - Store some arbitrary data in your Stellar account
  // manageData(quest_account);

  // challenge 04 - Add multisig to your account and make use of it in a transaction
  // addMultiSig(quest_account, custom_account.publicKey(), 1);
  bumpSequence(quest_account, '0', custom_account);

  // challenge 05 - Create a custom asset and send it to your account
  // createAccount(custom_account.secret(), '9000');
  // setTrustline(quest_account, HVLY);
  // sendPayment(custom_account, quest_account.publicKey(), HVLY, '10000000');

  // challenge 06 - Create an offer to sell your custom asset for XLM
  // let price = {
  //   n: 10, // XLM
  //   d: 1 // HVLY
  // }
  // manageSellOffer(quest_account, HVLY, Asset.native(), '1', price, 0);
}

main();
