# Stellar Quest Solutions

https://quest.stellar.org/

## Setup

1. Install [Nodejs](https://nodejs.org/en/) if you don't already have it. To check if Nodejs is installed on your machine open a terminal window and type the following command:
    - `node --version`
      - If you see something like `v12.18.4` you are good to go.
      - If you see something like `command not found: node` you need to download and install it before proceeding.
2. Download the project repo into a directory of your choice: https://gitlab.com/heavilyinvolved/stellar-quest
    - I chose `~/Dev/projects/stellar-quest/`
    - If you're using `git` you can run the following command from your project directory:
      - `git clone git@gitlab.com:heavilyinvolved/stellar-quest.git .`
3. Next install the [Stellar JS SDK](https://stellar.github.io/js-stellar-sdk/). From your project directory run the following command:
    - `npm install --save stellar-sdk`
4. Lastly, create a file named `config.json` and save it to your project directory. Add your private keys to the file:

    ```json
    {
      "keys": {
        "quest_account": "PASTE YOUR STELLAR QUEST PRIVATE KEY HERE",
        "custom_account": "PASTE YOUR CUSTOM ASSET ISSUER KEY HERE",
        "test_account": "PASTE ANY RANDOM KEYPAIR PRIVATE KEY"
      }
    }
    ```

    - `quest_account` keypair is provided by: https://quest.stellar.org/
      - This is your primary quest keypair and will be used to complete your quests.
    - `custom_account` keypair and `test_account` keypair can be generated here: https://laboratory.stellar.org/#account-creator?network=test


## Run
To begin your quest:

1. Open `quest.js` and navigate to the `main()` function.
2. Un-comment the quest you want to run leaving the other quests commented out. Save the file.
    - TODO: Add checks to each quest to only run them if they haven't already been successfully completed.
    - TODO: Make each quest return a Promise so we can just chain them together.
3. From the terminal run `node quest.js` from your project directory.
4. Have fun!
